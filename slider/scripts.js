let currentIndex = -1;
const elements = [...document.querySelectorAll('.element')]

const next = ( ) => {
    currentIndex++
    if ( currentIndex >= elements.length)
        currentIndex = 0
    const el = elements[currentIndex]
    let activeElement = document.querySelector('.element.active')
    if(activeElement){
        activeElement.classList.add('start-out')
        setTimeout(()=>{
            activeElement.classList.remove('active')
            activeElement.classList.remove('start-out')
        },500)
    }
    el.classList.add('active')
    setTimeout(()=>{
        next()
    },1000)
}
next()

